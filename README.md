# Clean Code & Design Patterns & MVC
## Pós Graduação -  UNIVEM



## Referências

Introdução

Dicionário do Programador
- Clean Code  - https://www.youtube.com/watch?v=ln6t3uyTveQ
- SOLID - https://www.youtube.com/watch?v=mkx0CdWiPRA
- Design Patterns -  https://www.youtube.com/watch?v=J-lHpiu-Twk
- Domain Driven Design - https://www.youtube.com/watch?v=GE6asEjTFv8

Uncle Bob - Clean Code

- Lesson 1 - https://www.youtube.com/watch?v=7EmboKQH8lM
- Lesson 2 - https://www.youtube.com/watch?v=2a_ytyt9sf8
- Lesson 3 - https://www.youtube.com/watch?v=Qjywrq2gM8o
- Lesson 4 - https://www.youtube.com/watch?v=58jGpV2Cg50
- Lesson 5 - https://www.youtube.com/watch?v=sn0aFEMVTpA
- Lesson 6 - https://www.youtube.com/watch?v=l-gF0vDhJVI


### Design Patterns

https://refactoring.guru/pt-br

[Christopher Okhravi](https://www.youtube.com/channel/UCbF-4yQQAWw-UnuCd2Azfzg)

- Strategy Pattern - https://www.youtube.com/watch?v=v9ejT8FO-7I&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc
- Observer Pattern - https://www.youtube.com/watch?v=_BpmfnqjgzQ&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=2
- Decorator Pattern - https://www.youtube.com/watch?v=GCraGHx6gso&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=3
- Factory Method Pattern - https://www.youtube.com/watch?v=EcFVTgRHJLM&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=4
- Abstract Factory Pattern - https://www.youtube.com/watch?v=v-GiuMmsXj4&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=5
- Singleton Pattern - https://www.youtube.com/watch?v=hUE_j6q0LTQ&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=6
- Command Pattern - https://www.youtube.com/watch?v=9qA5kw8dcSU&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=7
- Adapter Pattern - https://www.youtube.com/watch?v=2PKQtcJjYvc&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=9&t=0s
- Facade Pattern -  https://www.youtube.com/watch?v=K4FkHVO5iac&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=10&t=0s
- Proxy Pattern - https://www.youtube.com/watch?v=NwaabHqPHeM&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=11&t=0s
- Bridge Pattern - https://www.youtube.com/watch?v=F1YQ7YRjttI&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=12&t=2111s
- Template Method Pattern - https://www.youtube.com/watch?v=7ocpwK9uesw&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=14&t=0s
- Composite Pattern - https://www.youtube.com/watch?v=EWDmWbJ4wRA&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=15&t=0s
- Iterator Pattern - https://www.youtube.com/watch?v=uNTNEfwYXhI&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=17&t=0s
- State Pattern - https://www.youtube.com/watch?v=N12L5D78MAA&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=18&t=4171s
- Null Object Pattern - https://www.youtube.com/watch?v=rQ7BzfRz7OY&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc&index=19&t=3s



Domain Driven Design

 - https://medium.com/beelabacademy/domain-driven-design-vs-arquitetura-em-camadas-d01455698ec5



 https://blog.codinghorror.com/code-smells/