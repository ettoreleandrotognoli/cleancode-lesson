import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class JavaExample1  {

    interface ModelListener {
        void valueChanged(Model model);
    }

    interface View extends ModelListener {
        void setModel(Model model);
        void setVisible(boolean visible);
    }

    interface Model {
        void addListener(ModelListener listener);
    }

    interface ViewModelFactory {

        Model makeModel();

        View makeView();
    }

    static class Product implements Model{
        private final List<ModelListener> listeners = new ArrayList<>();


        protected void fireValueChanged() {
            for(ModelListener listener : listeners){
                listener.valueChanged(this);
            }
        }

        public void addListener(ModelListener listener) {
            listeners.add(listener);
        }

        private float value;

        public Product() {

        }

        public Product(float value ){
            this.value = value;
        }

        public void setValue(float value) {
            float oldValue = this.value;
            this.value = value;
            if(!Objects.equals(oldValue, value)){
                this.fireValueChanged();
            }
        }

        public float getValue() {
            return this.value;
        }

    }


    static class Customer implements Model {

        private final List<ModelListener> listeners = new ArrayList<>();


        protected void fireValueChanged() {
            for(ModelListener listener : listeners){
                listener.valueChanged(this);
            }
        }

        public void addListener(ModelListener listener) {
            listeners.add(listener);
        }

        private String name;

        public String getName() {
            return this.name;
        }

        public void setName(String name) {
            String oldValue = this.name;
            this.name = name;
            if(!Objects.equals(oldValue, name)){
                this.fireValueChanged();
            }
        }

        public Customer() {

        }

        public Customer(String name) {
            this.name = name;
        }

    }


    static class CustomerFrame extends JFrame implements View {

        Customer customer = null;
        JLabel nameLabel = new JLabel("Name:");
        JTextField nameTextField = new JTextField(20);

        private void init() {
            nameTextField.addKeyListener( new KeyAdapter() {
                public void keyReleased(KeyEvent e) {
                    if(customer == null) return;
                    customer.setName(nameTextField.getText());
                }
            });
            setLayout(new FlowLayout());
            add(nameLabel);
            add(nameTextField);
            pack();
            setLocationRelativeTo(null);
        }

        public CustomerFrame() {
            super();
            init();
        }

        public void setModel(Model customer) {
            this.customer = (Customer)customer;
            this.nameTextField.setText(this.customer.getName());
        }

        public void valueChanged(Model customer) {
            setModel(customer);
        }

    }

    static class ProductFrame extends JFrame implements View {

        Product product = null;
        JLabel valueLabel = new JLabel("Value:");
        JTextField valueTextField = new JTextField(20);

        private void init() {
            valueTextField.addKeyListener( new KeyAdapter() {
                public void keyReleased(KeyEvent e) {
                    if(product == null) return;
                    product.setValue(Float.parseFloat(valueTextField.getText()));
                }
            });
            setLayout(new FlowLayout());
            add(valueLabel);
            add(valueTextField);
            pack();
            setLocationRelativeTo(null);
        }

        public ProductFrame() {
            super();
            init();
        }

        public void setModel(Model product) {
            this.product = (Product)product;
            this.valueTextField.setText(String.valueOf(this.product.getValue()));
        }

        public void valueChanged(Model customer) {
            setModel(customer);
        }

    }
  

    static class CustomerViewModelFactory implements ViewModelFactory{

        public Model makeModel() {
            return new Customer();
        }

        public View makeView()  {
            return new CustomerFrame();
        }

    }

    static class ProductViewModelFactory implements ViewModelFactory{

        public Model makeModel() {
            return new Product();
        }

        public View makeView()  {
            return new ProductFrame();
        }
    }


    public static void main(String... args) {

        //ViewModelFactory abstractFactory = new ProductViewModelFactory();
        ViewModelFactory abstractFactory = new CustomerViewModelFactory();


        Model model = abstractFactory.makeModel();
        for(int i=0;i<10;i++){
            View frame = abstractFactory.makeView();
            frame.setModel(model);
            model.addListener(frame);
            frame.setVisible(true);
        }
    }

}