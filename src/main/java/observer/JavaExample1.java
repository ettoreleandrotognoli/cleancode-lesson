import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class JavaExample1  {



    interface CustomerListener {

        void valueChanged(Customer customer);

    }


    static class Customer {

        private final List<CustomerListener> listeners = new ArrayList<>();


        protected void fireValueChanged() {
            for(CustomerListener listener : listeners){
                listener.valueChanged(this);
            }
        }

        public void addListener(CustomerListener listener) {
            listeners.add(listener);
        }

        private String name;

        public String getName() {
            return this.name;
        }

        public void setName(String name) {
            String oldValue = this.name;
            this.name = name;
            if(!Objects.equals(oldValue, name)){
                this.fireValueChanged();
            }
        }

        public Customer() {

        }

        public Customer(String name) {
            this.name = name;
        }

    }

    static class CustomerFrame extends JFrame implements CustomerListener{

        Customer customer = null;
        JLabel nameLabel = new JLabel("Name:");
        JTextField nameTextField = new JTextField(20);

        private void init() {
            nameTextField.addKeyListener( new KeyAdapter() {
                public void keyReleased(KeyEvent e) {
                    if(customer == null) return;
                    customer.setName(nameTextField.getText());
                }
            });
            setLayout(new FlowLayout());
            add(nameLabel);
            add(nameTextField);
            pack();
            setLocationRelativeTo(null);
        }

        public CustomerFrame() {
            super();
            init();
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
            this.nameTextField.setText(customer.getName());
        }

        public void valueChanged(Customer customer) {
            setCustomer(customer);
        }

    }


    public static void main(String... args) {
        Customer customer = new Customer();
        for(int i=0;i<10;i++){
            CustomerFrame frame = new CustomerFrame();
            frame.setCustomer(customer);
            customer.addListener(frame);
            frame.setVisible(true);
        }
    }

}