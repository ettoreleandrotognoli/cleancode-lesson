---
marp: true
theme: uncover
---

<style>
section {
    text-align: left;
    font-size: 28px;
}
ul {
    margin-left: 0px;
}
section.center p, section.center ul, section.center li,
section.center h1,  section.center h2, section.center h3,
section.center h4,  section.center h5, section.center h6  {
    text-align: center;
}
section h1 {
    font-size: 40px;
}
section::after {
  font-weight: bold;
}
section.cover header{
    text-align: right;
    opacity: 0.1;
}
</style>

<!-- _class: center invert cover -->
# Clean Code & Design Patterns

<style scoped>
    section h1 {
        font-size: 55px;
    }
    footer {
        text-align: right;
    }
</style>

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->

Éttore Leandro Tognoli
ettore.leandro.tognoli@gmail.com

<!-- _footer: Powered by [Marp](https://yhatt.github.io/marp/) & [PlantUML](https://github.com/plantuml) -->


---

<!-- paginate: true -->

<!-- _footer: https://github.com/ettoreleandrotognoli -->

![bg right fit ](resources/ettore.jpg)


Bacharel em Ciência da Computação - UNIVEM

Mestre em Ciência da Computação - UFSCar

Engenheiro de Software - RedHat

Trabalhei em N lugares em Marília e região como CLT e/ou PJ

---
<!-- _class: center invert -->

Não estou aqui representando a RedHat

---

# Agenda

Motivação
Revisão de POO
Clean Code
SOLID
Design Patterns
MVC

---

# Extreme Go Horse

![bg 20%](resources/go-horse.svg)


<!-- _footer: https://gohorseprocess.com.br/extreme-go-horse-xgh/ -->

---


# Os 2 Valores do Software

Comportamento & Estrutura
Escreva software não firmware

"_Every software system provides two different values to the stakeholders: behavior and structure. Software developers are responsible for ensuring that both those values remain high. Unfortunately, they often focus on one to the exclusion of the other. Even more unfortunately, they often focus on the lesser of the two values, leaving the software system eventually valueless._"

---

<!-- _class: center invert -->
<!-- header: Revisão de POO -->

# Revisão de POO
POO


Programação Orientada a Objetos

---

Classes, Objetos, Atributos, Métodos
Modificadores de acesso ( public, protected, private, default )
Modificadores ( final, static, abstract )

---


Eu aprendi POO errado
Conhecei várias pessoas no mercado que não sabiam ou que tinham aprendido errado também

---

Dificuldades em aceitar as frases?

- Prefira composição ao invés de herança
- Prefira polimorfismo ao invés de switch cases
- ...
- Programe para interfaces

Também já tive ¯\\\_(ツ)_/¯

---

# Alan Kay

Alan Curtis Kay (Springfield, 17 de maio de 1940) é um informático estadunidense. É conhecido por ter sido um dos inventores da linguagem de programação Smalltalk, e um dos pais do conceito de programação orientada a objetos, que lhe valeu o Prêmio Turing em 2003. Concebeu o laptop e a arquitetura das modernas interfaces gráficas dos computadores (GUI).


<!-- _footer: https://pt.wikipedia.org/wiki/Alan_Kay -->

---

# Smalltalk

O projeto Smalltalk foi influenciado pela analogia de Kay baseada em princípio biológicos e algébricos. Ela idealiza entidades individuais, ou células, que se comunicam entre si pela troca de mensagens. De certa forma, a linguagem Smalltalk pode ser considerada como a mãe das linguagens orientadas a objetos.

<!-- _footer: http://www.nuted.ufrgs.br/oa/animak/teorias_alan_kay.php -->

---

<!-- _footer: https://pt.wikipedia.org/wiki/Jogo_da_vida -->

# Sistema Complexo

![bg right fit](resources/game-of-life.gif)

"...
_Simple is better than complex._
_Complex is better than complicated._
..."
The Zen of Python

---

# Polimorfismo

Cansado do exemplo com animais? :cat: :dog: :duck:

![bg right fit](resources/animals-uml.svg)

---

# Peças de um Quebra-Cabeças

![bg right fit](resources/puzzle-1.svg)

---

# Herança

Herda as bordas a interface pública
O reúso vem das combinações e não da herança do conteúdo

![bg right fit](resources/puzzle-1-uml.svg)

---

# USB Mass Storage Device

- "HD" Externo
    - SSD
    - Disco Magnético
- Pendrive
- Adaptador de Cartão SD

Não tem um switch case no seu SO para decidir como controlar a agulha magnetica do HD, esse comportamento é responsabilidade do HD.
Todos os dipositivos fornecem uma interface padrão para "acessar arquivos", ocultando seu funcionamento interno.

---


# Ocultação de Dados/Informação

Esse conceito também é aplicável em linguagens que não são orientadas a objetos.

O cliente só precisa/deve ter conhecimento da interface, os detalhes internos são escondidos.

---


# RGB & HSV

Existem operações no domínio do HSV e do RGB

![bg left contain](./resources/rgb-model.svg)
![bg right contain](./resources/hsv-model.jpg)

---

# RGB & HSV

Somar valores é mais trivial com RGB
Identificar cores é mais trivial com HSV

Usando qual estrutura de dados ( RGB vs HSV ) devo armazenar ( memoria, disco, ... ) a cor?

---

<!-- _class: center invert -->
<!-- header: Clean Code -->

# Clean Code

---

<!-- _footer: https://en.wikipedia.org/wiki/Robert_C._Martin -->

# Uncle Bob

![bg right fit](resources/uncle-bob.jpg)

Robert Cecil Martin

Atua desde 1970
Um dos signatários do [Manifesto Ágil](http://agilemanifesto.org/)

- Clean Code
- The Clean Coder
- Clean Architecture

---

# 

"_A quantia de tempo gasto lendo código versus escrevendo é bem mais de 10 para 1_"
Robert C. Martin

"_Readability counts._"
The Zen of Python

Qualquer um escreve código que uma máquina entende, difícil é escrever código que outros humanos entendam

---

Acho que ninguém gosta de ler algo desse tipo

![bg right fit](./resources/hadouken-indentation.jpg)

<!-- _footer: https://diogommartins.wordpress.com/2016/08/22/como-se-defender-de-ifs-hadouken/ -->

---

# Early Return/Except

```java

Pair<Double,Double> bhaskara(double a,double b, double c) {
    double delta = b * b - 4 * a * c;
    if( delta >= 0 ) {
        return new Pair(
            (-b + Math.sqrt(delta)) / 2 * a,
            (-b - Math.sqrt(delta)) / 2 * a
        );
    }
    else {
        throw new RuntimeException();
    }
}

```

---

# Early Return/Except

```java

Pair<Double,Double> bhaskara(double a,double b, double c) {
    double delta = b * b - 4 * a * c;
    if( delta < 0 ){
        throw new RuntimeException();
    }
    return new Pair(
        (-b + Math.sqrt(delta)) / 2 * a,
        (-b - Math.sqrt(delta)) / 2 * a
    );
}

```

---

Cuidado com justificativas:

- Da no mesmo
- Mas cada um pensa de um jeito
- Depois eu arrumo/melhoro
- Mas está funcionando

---


#

"_Special cases aren't special enough to break the rules._"
The Zen of Python

[Linus TED](https://youtu.be/Vo9KPk-gqKk?t=860)

---



# Bad/Code Smells

![bg right fit](./resources/refactoring-to-patterns-cover.jpg)

Catalogo de problemas e formas de refatorar


<!-- _footer: https://martinfowler.com/books/r2p.html -->

---

Train Wrecks
Middle Man
[Classe/Objeto deus](https://en.wikipedia.org/wiki/God_object)

---

Train Wrecks

```java


class GatewayVerifier {

    public boolean isPaymentGatewayAvailable(Client client) {
        return client.getBasket().getPaymentType().getGateway().isAvailable();
    }

}

```

---

![bg fit](./resources/train-wrecks-uml.svg)

![bg fit](./resources/full-train-wrecks-uml.svg)

---

Ferramentas

- Sonarqube - https://www.sonarqube.org/
- Codeclimate - https://codeclimate.com/
- Codefactor - https://www.codefactor.io/

---

![bg right fit](./resources/clean-code-cover.jpg)

# Clean Code

---

# Nomes Significativos
- Buscáveis
- Revelam Intenção
- Evite codificação ( prefixos de membros, notação hungra )
- Use nomes do dominio

---

# Funções
- Nome
    - Verbos
    - Ver nomes significativos
- Pequenas e menores que isso
- Fazer apenas uma coisa
- Poucos Argumentos ( ideal um só)
- Sem Efeitos Colaterais
- Evitar uso de argumentos como resposta
- Comando ou Consulta, não os dois
- Exceções não códigos de erro
- DRY
---

# Comentários

- Explique-se com o código
- Se você sentiu necessisade de comentar o código, você já falhou com sua expressevidade
- O melhor comentário é aquele que você arrumou um jeito de não escrever

---

Por padrão os programadores não esperam que comentários alterem o comportamento do código, na pressa ninguém se preocupa em mantem um comentário atualizado.

---

Fica também minha crítica ao Doctrine (ORM) do PHP
Mas sim é uma solução alternativa interessante para anotações

```php
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="products")
 */
class Product {
    /** 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    /** 
     * @ORM\Column(type="string") 
     */
    protected $name;
}

```

---


# The Clean Coder

![bg right fit](./resources/clean-coder-cover.jpg)


---

# Clean Architecture

![bg right fit](./resources/clean-architecture-cover.jpg)

As novas linguagens/paradgmas restringem o programador, tiram a liberdade de escrever coisas "erradas"


---

Importância da Imutabilidade ( programação funcional )

Strings em Java - Economia de Memoria
Concorrência de Rust - Evitar condições de corrida

---

<!-- _class: center invert -->
<!-- header: SOLID -->

# SOLID

---

S - SRP - Single Responsability Principle
O - OCP - Open Close Principle
L - LSP - Liskov Substitution Principle
I - ISP - Interface Segregation Principle
D - DIP - Dependency Inversion Principle

---


# SRP

Single Responsability Principle

---

# OCP

Open Close Principle

Aberto para expansão e fechados para modificação

Pense eu reúso, alguém ou até mesmo você será "cliente" desse código.

---

# LSP

Liskov Substitution Principle


---

# Barbara Liskov

![bg right fit](resources/liskov.jpg)

<!-- _footer: https://pt.wikipedia.org/wiki/Barbara_Liskov -->

Inventou o Tipo Abstrato de Dado 

Primeira mulher a obter um PhD em Ciência da Computação nos Estados Unidos

---

# LSP

![bg right fit](./resources/what-the-fuck.jpg)

Se q(x) é uma propriedade demonstrável dos objetos x de tipo T. Então q(y) deve ser verdadeiro para objetos y de tipo S onde S é um subtipo de T

---

# ISP

Interface Segregation Principle

---

# DIP

Dependency Inversion Principle

---


<!-- _class: center invert -->
<!-- header: Design Patterns -->

# Design Patterns

---

# Design Patterns

[Erich Gamma](https://en.wikipedia.org/wiki/Erich_Gamma)
[Richard Helm](https://wiki.c2.com/?RichardHelm)
[Ralph Johnson](https://en.wikipedia.org/wiki/Ralph_Johnson_(computer_scientist))
[John Vlissides](https://en.wikipedia.org/wiki/John_Vlissides)

GoF - Gang of Four

![bg right fit](resources/design-patterns.jpg)


---

Creational Patterns ( 5 )
Structural Patterns ( 7 )
Behavioral Patterns ( 11 )

---

<!-- _class: center invert -->

# Creational Patterns

---

<!-- header: Design Patterns - Creational Patterns -->


- Abstract Factory
- Builder
- Factory Method
- Prototype
- Singleton

---

<!-- _class: center invert -->

# Singleton

---

<!-- header: Design Patterns - Creational Patterns - Singleton -->

"_Ensure a class only has one instance, and provide a global point of access to it._"



"_Singleton é um (anti-)padrão de projeto de software (do inglês Design Pattern). Este padrão garante a existência de apenas uma instância de uma classe, mantendo um ponto global de acesso ao seu objeto._"


<!-- _footer: https://pt.wikipedia.org/wiki/Singleton -->

---

```php

class SingletonClass {

    private static $instance = null;


    private function __construct() {

    }

    public function getInstance() {
        if(self::$instance == null) {
            self::$instance = new SingletonClass();
        }
        return self::$instance;
    }

}

```

---

![bg left fit](./resources/no.png)

```php

class A {
    public function a() {
        SingletonClass.getInstance();
    }
}

class B {
    public function b() {
        SingletonClass.getInstance();
    }
}


class Z {
    public function z() {
        SingletonClass.getInstance();
    }
}

```

---

Lembre-se de DIP ( Dependency Inversion Principle )
Lembre-se de SRP ( Single Responsability Principle )
Frameworks de Injeção de Dependência gerenciam as instâncias

---

![bg left fit](./resources/yes.png)

```php

interface Service {}

class ServiceImplA implements Service {
    /** singleton code **/
}

class ServiceImplB implements Service {
    /** singleton code **/
}


class Client {
    public function __construct(
        Service $service
        ) {
        /** set code **/
    }
}

$client = new Client(
    ServiceImplA.getInstance()
);
$client->consumeService();
$client = new Client(
    ServiceImplB.getInstance()
);
$client->consumeService();

```

---

Dependendo do caso da para usar um enum no lugar ¯\\\_(ツ)_/¯

---

<!-- header: Design Patterns - Creational Patterns -->

<!-- _class: center invert -->

# Factory Method


---

<!-- header: Design Patterns - Creational Patterns - Factory Method -->

"_Define an interface for creating an object, but let subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses._"

---

<!-- header: Design Patterns - Creational Patterns -->

<!-- _class: center invert -->

# Builder

---

<!-- header: Design Patterns - Creational Patterns - Builder -->

"_Separate the construction of a complex object from its representation so that the same construction process can create different representations._" 

---


Uso comum de builder:

```java

User user = User.newBuilder()
    .setName("Fuu")
    .setEmail("fuu@fuu.com")
    .setUsername("fuu")
    .build();

```

Isso é um caso de Train Wrecks?

---

<!-- header: Design Patterns - Creational Patterns -->

<!-- _class: center invert -->

# Prototype

---


<!-- header: Design Patterns - Creational Patterns - Prototype -->

"_Specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype._"

---


Javascript Prototype?

<!-- _footer: https://www.w3schools.com/js/js_object_prototypes.asp -->

---


<!-- header: Design Patterns - Creational Patterns -->

<!-- _class: center invert -->


# Abstract Factory

---

<!-- header: Design Patterns - Creational Patterns - Abstract Factory -->

"_Provide an interface for creating families of related or dependent objects without specifying their concrete classes._"

---


<!-- header: Design Patterns -->
<!-- _class: center invert -->


# Structural Patterns

---

<!-- header: Design Patterns - Structural Patterns -->

- Adapter
- Bridge
- Composite
- Decorator
- Facade
- Flyweight
- Proxy

---

<!-- header: Design Patterns - Structural Patterns -->
<!-- _class: center invert -->

# Decorator

---

<!-- header: Design Patterns - Structural Patterns - Decorator -->

"_Attach additional responsibilities to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality._"

---


![bg fit](./resources/decorator-uml.svg)

---

Python Decorator?

Não necessáriamente é usado para implementar o padrão decorator

Bem diferente de anotações do java

```python

@algum_decorator
def funcao():
    pass

@algum_class_decorator
class Classe():
    pass

```

---

Exemplo como decorator:

```python
def log(func):
    def wrapper(*args,**kwargs):
        print("{}(args={}, kwargs={})".format(func.__name__, args, kwargs))
        result = func(*args,**kwargs)
        print("{}(args={}, kwargs={}) -> {}".format(func.__name__, args, kwargs, result))
        return result
    return wrapper

@log
def hello(name):
    return print("Hello {}!".format(name))

"""
syntactic sugar for:
    hello = log(hello)
"""
```

---

![bg fit](./resources/decorator-logger-uml.svg)

---


<!-- header: Design Patterns -->
<!-- _class: center invert -->

# Behavioral Patterns


---

<!-- header: Design Patterns - Behavioral Patterns -->

- Chain of Responsibility
- Command
- Interpreter
- Iterator
- Mediator
- Memento
- Observer
- State
- Strategy
- Template Method
- Visitor

---


<!-- header: Design Patterns - Behavioral Patterns-->
<!-- _class: center invert -->

# Chain of Responsability

---

<!-- header: Design Patterns - Behavioral Patterns - Chain of Responsability -->

"_Avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. Chain the receiving objects and pass the request along the chain until an object handles it._"

---

Matador de if complexos

![bg left fit](./resources/no.png)

```python
if request.path.startswith('/home'):
    if request.path == '/home':
        pass
    elif request.path == '/home/about':
        pass
elif request.path.startwith('/profile'):
    if request.method == 'POST':
        pass
    elif request.method == 'GET':
        pass
```

---

![bg left fit](./resources/yes.png)

```python

@get('/home')
def home(request):
    pass

@get('/home/about')
def home_about(request):
    pass


@post('/profile')
def create_profile(request):
    pass

@get('/profile')
def show_profile(request):
    pass

```


---

<!-- header: Design Patterns - Behavioral Patterns-->
<!-- _class: center invert -->


# Observer


---

<!-- header: Design Patterns - Behavioral Patterns - Observer -->

"_Define a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically._"

---

Muito utilizado em UI e operações assíncronas ( promise, future )

Composição do comportamento

Mudança de pensamento ( faça Y quando X acontecer )



---

![bg right fit](./resources/reactivex.png)

"_ReactiveX is a combination of the best ideas from the Observer pattern, the Iterator pattern, and functional programming_"

---

<!-- header: Design Patterns - Behavioral Patterns-->
<!-- _class: center invert -->

# Strategy

---

<!-- header: Design Patterns - Behavioral Patterns - Strategy -->

"_Define a family of algorithms, encapsulate each one, and make theminterchangeable. Strategy lets the algorithm vary independently fromclients that use it._"

---


<!-- _class: center invert -->
<!-- header: MVC -->

# MVC

Model View Controller

---

![bg right fit](./resources/first-mvc.png)

Origem do MVC no smalltalk

<!-- _footer: https://givan.se/mvc-past-present-and-future/ -->

---

#

![bg left fit](./resources/wrong-mvc-uml.svg)
![bg right fit](./resources/what-the-fuck.jpg)

Forte acoplamento entre camadas

Como reúsar?

---


"_In MVC, the domain element is referred to as the model. Model objects are completely ignorant of the UI._"

<!-- _footer: https://martinfowler.com/eaaDev/uiArchs.html#ModelViewController -->

---

![bg fit](./resources/yes.png)
![bg fit](./resources/mvc-uml.svg)

---

<!-- _class: center invert -->

# Domain Driven Design & Clean Architecture

---

![bg right fit](./resources/domain-driven-design-cover.jpg)

---


Clean Architecture

- Domínio deve ser independente de detalhes de implementação

- Arquitetura Gritante

Domain Driven Design

- Domínio deve ser independente da camada de infra

-  Linguagem Ubíqua/Onipresente/Comum

---

Frameworks resolvem grande parte dos problemas de infra e aceleram o desenvolvimento.

Quanto/devo acoplar a camada de domínio/modelo com o framework?


---

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->
<!-- paginate: false -->
<!-- _class: center invert cover -->


# Obrigado!


Éttore Leandro Tognoli
ettore.leandro.tognoli@gmail.com